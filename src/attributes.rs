use std::collections::HashSet;
use url::Url;
mod language;
use chrono;

// modelling all the attributes in this cheatsheet
// https://www.w3.org/2010/04/xhtml10-strict.html
// (2024/05/14) update, for the time being I've changed my mind to only model the things that I want
// maybe later I'll expand the scope to be a complete implementation of xhtml
pub trait Attribute {
    fn to_xhtml_string(&self) -> String;
}

#[derive(Clone)]
pub struct EventAttributes {
    on_mouse_up: Option<OnMouseUp>,
    on_mouse_out: Option<OnMouseOut>,
    on_key_press: Option<OnKeyPress>,
    on_key_down: Option<OnKeyDown>,
    on_mouse_down: Option<OnMouseDown>,
    on_mouse_move: Option<OnMouseMove>,
    on_mouse_over: Option<OnMouseOver>,
    on_click: Option<OnClick>,
    on_key_up: Option<OnKeyUp>,
    on_dbl_click: Option<OnDblClick>,
}

#[derive(Clone)]
pub struct OnMouseUp(String);
impl Attribute for OnMouseUp {
    fn to_xhtml_string(&self) -> String {
        format!("onmouseup=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnMouseOut(String);
impl Attribute for OnMouseOut {
    fn to_xhtml_string(&self) -> String {
        format!("onmouseout=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnKeyPress(String);
impl Attribute for OnKeyPress {
    fn to_xhtml_string(&self) -> String {
        format!("onkeypress=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnKeyDown(String);
impl Attribute for OnKeyDown {
    fn to_xhtml_string(&self) -> String {
        format!("onkeydown=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnMouseDown(String);
impl Attribute for OnMouseDown {
    fn to_xhtml_string(&self) -> String {
        format!("onmousedown=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnMouseMove(String);
impl Attribute for OnMouseMove {
    fn to_xhtml_string(&self) -> String {
        format!("onmousemove=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnMouseOver(String);
impl Attribute for OnMouseOver {
    fn to_xhtml_string(&self) -> String {
        format!("onmouseover=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnClick(String);
impl Attribute for OnClick {
    fn to_xhtml_string(&self) -> String {
        format!("onclick=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnKeyUp(String);
impl Attribute for OnKeyUp {
    fn to_xhtml_string(&self) -> String {
        format!("onkeyup=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct OnDblClick(String);
impl Attribute for OnDblClick {
    fn to_xhtml_string(&self) -> String {
        format!("ondblclick=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct Style(String);
impl Attribute for Style {
    fn to_xhtml_string(&self) -> String {
        format!("style=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct XmlLang(language::Code);
impl Attribute for XmlLang {
    fn to_xhtml_string(&self) -> String {
        format!("xml:lang=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
pub struct Xmlns;
impl Attribute for Xmlns {
    fn to_xhtml_string(&self) -> String {
        format!("xmlns=\"http://www.w3.org/1999/xhtml\"")
    }
}

//TODO  rather than representing classes in strings you should model them

#[derive(Clone)]
pub struct Class(HashSet<String>);
impl Attribute for Class {
    fn to_xhtml_string(&self) -> String {
        let class_string = self.0.iter().fold("".to_string(), |output, class_name| {
            output + " " + class_name
        });
        format!("class=\"{}\"", &class_string[1..])
    }
}

#[derive(Clone)]
pub struct Id(String);
impl Attribute for Id {
    fn to_xhtml_string(&self) -> String {
        format!("id=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct Title(String);
impl Attribute for Title {
    fn to_xhtml_string(&self) -> String {
        format!("title=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct XmlSpace(XmlSpaceValue);
impl Attribute for XmlSpace {
    fn to_xhtml_string(&self) -> String {
        format!("xml:space=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
enum XmlSpaceValue {
    Default,
    Preserve,
}

impl XmlSpaceValue {
    fn to_string(&self) -> String {
        match self {
            Self::Default => "default".to_string(),
            Self::Preserve => "preserve".to_string(),
        }
    }
}

#[derive(Clone)]
pub struct Abbr(String);
impl Attribute for Abbr {
    fn to_xhtml_string(&self) -> String {
        format!("abbr=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct Accept(AcceptValue);
impl Attribute for Accept {
    fn to_xhtml_string(&self) -> String {
        format!("accept=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
enum AcceptValue {
    TextPlain,
    TextHtml,
    TextCss,
    TextXml,
    ApplicationXml,
    ApplicationXhtmlXml,
    ApplicationJson,
    MultipartFormData,
    ImagePng,
    ImageJpeg,
    ImageGif,
}
impl ToString for AcceptValue {
    fn to_string(&self) -> String {
        match self {
            AcceptValue::TextPlain => "text/plain".to_string(),
            AcceptValue::TextHtml => "text/html".to_string(),
            AcceptValue::TextCss => "text/css".to_string(),
            AcceptValue::TextXml => "text/xml".to_string(),
            AcceptValue::ApplicationXml => "application/xml".to_string(),
            AcceptValue::ApplicationXhtmlXml => "application/xhtml+xml".to_string(),
            AcceptValue::ApplicationJson => "application/json".to_string(),
            AcceptValue::MultipartFormData => "multipart/form-data".to_string(),
            AcceptValue::ImagePng => "image/png".to_string(),
            AcceptValue::ImageJpeg => "image/jpeg".to_string(),
            AcceptValue::ImageGif => "image/gif".to_string(),
        }
    }
}

//TODO: Make it so that this takes in a Charset Value rather than just any string

#[derive(Clone)]
pub struct AcceptCharset(String);
impl Attribute for AcceptCharset {
    fn to_xhtml_string(&self) -> String {
        format!("accept=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct Action(ActionValue);

impl Attribute for Action {
    fn to_xhtml_string(&self) -> String {
        format!("action=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
enum ActionValue {
    GET,
    POST,
    Url(Url),
    Empty,
}

impl ToString for ActionValue {
    fn to_string(&self) -> String {
        match self {
            ActionValue::GET => "get".to_string(),
            ActionValue::POST => "post".to_string(),
            ActionValue::Url(url) => url.to_string(),
            ActionValue::Empty => "".to_string(),
        }
    }
}

#[derive(Clone)]
pub struct Align(AlignValue);

impl Attribute for Align {
    fn to_xhtml_string(&self) -> String {
        format!("align=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
enum AlignValue {
    Left,
    Center,
    Right,
    Justify,
    Char,
}

impl ToString for AlignValue {
    fn to_string(&self) -> String {
        match self {
            AlignValue::Left => "left".to_string(),
            AlignValue::Center => "center".to_string(),
            AlignValue::Right => "right".to_string(),
            AlignValue::Justify => "justify".to_string(),
            AlignValue::Char => "char".to_string(),
        }
    }
}

// (2024/05/14 7:27午前)
// the attributes that you will do
// alt, checked, datetime, disabled, href, readonly, selected, src, value

#[derive(Clone)]
pub struct Alt(String);

impl Attribute for Alt {
    fn to_xhtml_string(&self) -> String {
        format!("alt=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct Checked;

impl Attribute for Checked {
    fn to_xhtml_string(&self) -> String {
        format!("\'checked\'")
    }
}

#[derive(Clone)]
pub struct Datetime(chrono::DateTime<chrono::Utc>);
impl Attribute for Datetime {
    fn to_xhtml_string(&self) -> String {
        format!("datetime=\"{}\"", self.0.to_rfc3339())
    }
}

#[derive(Clone)]
pub struct Disabled;

impl Attribute for Disabled {
    fn to_xhtml_string(&self) -> String {
        format!("\'disabled\'")
    }
}

#[derive(Clone)]
pub struct Href(Url);

impl Attribute for Href {
    fn to_xhtml_string(&self) -> String {
        format!("href=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
pub struct Readonly;

impl Attribute for Readonly {
    fn to_xhtml_string(&self) -> String {
        format!("\'readonly\'")
    }
}

#[derive(Clone)]
pub struct Selected;

impl Attribute for Selected {
    fn to_xhtml_string(&self) -> String {
        format!("\'selected\'")
    }
}

#[derive(Clone)]
pub struct Src(Url);

impl Attribute for Src {
    fn to_xhtml_string(&self) -> String {
        format!("Src=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
pub struct Value(String);

impl Attribute for Value {
    fn to_xhtml_string(&self) -> String {
        format!("value=\"{}\"", self.0)
    }
}

#[derive(Clone)]
pub struct Rel(RelValue);

impl Attribute for Rel {
    fn to_xhtml_string(&self) -> String {
        format!("rel=\"{}\"", self.0.to_string())
    }
}

#[derive(Clone)]
enum RelValue {
    Stylesheet,
    Icon,
    Alternate,
    Next,
    Prev,
    Tag,
    NoReferrer,
    Author,
    NoFollow,
    Help,
}

impl ToString for RelValue {
    fn to_string(&self) -> String {
        match self {
            RelValue::Stylesheet => "stylesheet".to_string(),
            RelValue::Icon => "icon".to_string(),
            RelValue::Alternate => "alternate".to_string(),
            RelValue::Next => "next".to_string(),
            RelValue::Prev => "prev".to_string(),
            RelValue::Tag => "tag".to_string(),
            RelValue::NoReferrer => "noreferrer".to_string(),
            RelValue::Author => "author".to_string(),
            RelValue::NoFollow => "nofollow".to_string(),
            RelValue::Help => "help".to_string(),
        }
    }
}
