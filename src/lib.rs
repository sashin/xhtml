mod attributes;

mod elements;
use std::collections::{HashMap, HashSet};
use url::Url;

// (2024/05/14 10:13午前) most of what you've done in this document you will replace with the elements module

// maybe you should make a trait called node
// learn about traits again
pub trait node {
    fn to_xhtml(&self) -> String;
}

// go through everything recursively and add all the valid children and attributes
enum Node {
    Html(Html),
    Head(Head),
    Title,
    Body(Body),
    Meta(Meta),
    Link(Link),
    Script,
    Style(Style),
    P,
    Div,
    Span,
    Pre,
    Ul,
    Ol,
    Li,
    A,
    Hr,
    Br,
    Img,
    Table,
    Tr,
    Th,
    Td,
    Form,
    Input,
    TextArea,
    Select,
    Option,
    Button,
    Label,
    Text,
    Comment,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
}

// These are the attributes common to every tag,
//if the tag allows other attributes there will be a TagAttributes structure that has one field called common which takes this
struct CommonAttributes {
    id: Option<String>,
    classes: HashSet<String>,
    title: Option<String>,
    lang: Option<Language>,
    dir: Option<Direction>,
    xml_lang: Option<Language>,
    xml_space: Option<WhiteSpaceHandling>,
    //you want to be able to model styles more precisely later
    // this can be saved for when you make xcss as a library
    style: HashMap<String, String>,
}

//todo add more languages
enum Language {
    En,
}

enum Direction {
    RTL,
    LTR,
}

enum WhiteSpaceHandling {
    Default,
    Preserve,
}

struct Html {
    attributes: CommonAttributes,
    children: HtmlChildren,
}

struct HtmlChildren {
    head: Option<Head>,
    body: Option<Body>,
}

struct Head {
    children: Vec<HeadChildren>,
}

struct HeadChildren {
    link: Vec<Link>,
    meta: Vec<Meta>,
    //model styles more precisely later
    style: Vec<Style>,
}

struct Link {
    attributes: LinkAttributes,
}

struct LinkAttributes {
    href: Option<Url>,
    rel: Option<Relation>,
    href_lang: Option<Language>,
    link_type: Option<LinkType>,
    media: Option<MediaType>,
}

enum Relation {
    Stylesheet,
    Icon,
    Alternate,
    Next,
    Prev,
}

enum LinkType {
    TextCss,
    ImageXIcon,
    TextPlain,
    TextHtml,
    ApplicationXhtmlXml,
    ApplicationXml,
    ApplicationRssXml,
    ImagePng,
    ImageJpeg,
    ImageGif,
    ApplicationJSON,
}

enum MediaType {
    All,
    Screen,
    Print,
    TV,
    Projection,
    Handheld,
    Braille,
    Aural,
    TTY,
}

struct Meta {
    attributes: MetaAttributes,
}

struct MetaAttributes {
    name: Option<MetaName>,
    content: Option<String>,
    http_equiv: HttpEquiv,
}

enum MetaName {
    Description,
    Keywords,
    Author,
    Viewport,
    FormatDetection,
    ThemeColor,
}

struct HttpEquiv {
    content_type: bool,
    refresh: bool,
}

struct Style {
    attributes: StyleAttributes,
}

struct StyleAttributes {
    style_type: Option<StyleType>,
}

enum StyleType {
    TextCss,
    TextScss,
    TextLess,
    TextStylus,
}

struct Body {
    attributes: CommonAttributes,
    children: Vec<BodyChild>,
}

enum BodyChild {
    Div(Div),
}

struct Div {
    children: Vec<DivChild>,
    attributes: CommonAttributes,
}

enum DivChild {
    A,
    BlockQuote,
    Br,
    Cite,
    Code,
    Em,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    Hr,
    P,
    Pre,
    Span,
}

impl Node {
    //this might not be necessary, the absence of a children field may imply this
    fn is_self_closing(&self) -> bool {
        match self {
            Node::Meta(_) => true,
            Node::Link(_) => true,
            Node::Img => true,
            Node::Hr => true,
            Node::Br => true,
            _ => false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = 4;
        assert_eq!(result, 4);
    }
}
