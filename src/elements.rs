use std::fmt::Display;

use crate::attributes::{self, Attribute};

struct CommonAttributes {
    id: Option<attributes::Id>,
    class: Option<attributes::Class>,
    title: Option<attributes::Title>,
    style: Option<attributes::Style>,
    xml_lang: Option<attributes::XmlLang>,
    xml_space: Option<attributes::XmlSpace>,
}

impl ToString for CommonAttributes {
    fn to_string(&self) -> String {
        let mut output = "".to_string();

        if let Some(id) = self.id.clone() {
            output += &format!("id=\"{}\" ", id.to_xhtml_string());
        };

        if let Some(class) = self.class.clone() {
            output += &format!("class=\"{}\" ", class.to_xhtml_string());
        };

        if let Some(title) = self.title.clone() {
            output += &format!("title=\"{}\" ", title.to_xhtml_string());
        };

        if let Some(style) = self.style.clone() {
            output += &format!("style=\"{}\" ", style.to_xhtml_string());
        };
        if let Some(xml_lang) = self.xml_lang.clone() {
            output += &format!("xml:lang=\"{}\" ", xml_lang.to_xhtml_string());
        };
        if let Some(xml_space) = self.xml_space.clone() {
            output += &format!("xml:space=\"{}\" ", xml_space.to_xhtml_string());
        };

        output
    }
}

struct Html {
    attributes: CommonAttributes,
    children: HtmlChildren,
}
struct HtmlChildren {
    head: Option<Head>,
    body: Option<Body>,
}
struct Head {
    children: Vec<HeadChildren>,
}

struct HeadChildren {
    link: Vec<Link>,
    meta: Vec<Meta>,
    style: Vec<Style>,
}
struct Link {
    attributes: LinkAttributes,
}

struct LinkAttributes {
    href: Option<attributes::Href>,
    rel: Option<attributes::Rel>,
    href_lang: Option<attributes::HrefLang>,
    link_type: Option<LinkType>,
    media: Option<MediaType>,
}
